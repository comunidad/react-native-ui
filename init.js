import { Theme } from '@corvus/react-native-theme';
import getThemeStyle from './theme';

function setDefaultThemeStyle() {
  const theme = getThemeStyle();
  Theme.setDefaultThemeStyle(theme);
}

export {
  setDefaultThemeStyle,
};
