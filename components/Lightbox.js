import { default as Lightbox } from 'react-native-lightbox';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

const AnimatedLightbox = connectAnimation(Lightbox);
const StyledLightbox = connectStyle('corvus.ui.Lightbox')(AnimatedLightbox);

export {
  StyledLightbox as Lightbox,
};
