import React, { Component } from 'react';
import { Text as RNText } from 'react-native';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

class Text extends Component {
  render() {
    return (
      <RNText {...this.props} />
    );
  }
}

Text.propTypes = {
  ...RNText.propTypes,
};

const AnimatedText = connectAnimation(Text);
const StyledText = connectStyle('corvus.ui.Text')(AnimatedText);
const Heading = connectStyle('corvus.ui.Heading')(AnimatedText);
const Title = connectStyle('corvus.ui.Title')(AnimatedText);
const Subtitle = connectStyle('corvus.ui.Subtitle')(AnimatedText);
const Caption = connectStyle('corvus.ui.Caption')(AnimatedText);

export {
  StyledText as Text,
  Heading,
  Title,
  Subtitle,
  Caption,
};
