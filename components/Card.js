import { View } from 'react-native';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

const AnimatedCard = connectAnimation(View);
const Card = connectStyle('corvus.ui.Card')(AnimatedCard);

export {
  Card,
};
