import { View } from 'react-native';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

const AnimatedScreen = connectAnimation(View);
const Screen = connectStyle('corvus.ui.Screen')(AnimatedScreen);

export {
  Screen,
};
