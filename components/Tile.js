import { View } from 'react-native';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

const AnimatedTile = connectAnimation(View);
const Tile = connectStyle('corvus.ui.Tile')(AnimatedTile);

export {
  Tile,
};
