import { View } from 'react-native';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

const AnimatedDivider = connectAnimation(View);
const Divider = connectStyle('corvus.ui.Divider')(AnimatedDivider);

export {
  Divider,
};
