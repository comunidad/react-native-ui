import { View } from 'react-native';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

const AnimatedRow = connectAnimation(View);
const Row = connectStyle('corvus.ui.Row')(AnimatedRow);

export {
  Row,
};
