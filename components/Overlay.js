import { View } from 'react-native';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

const AnimatedOverlay = connectAnimation(View);
const Overlay = connectStyle('corvus.ui.Overlay')(AnimatedOverlay);

export {
  Overlay,
};
