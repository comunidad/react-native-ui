import React, { Component } from 'react';

import { connectStyle } from '@corvus/react-native-theme';
import { connectAnimation } from '@corvus/react-native-animation';

import { View } from './View';

class FormGroup extends Component {
  render() {
    return (
      <View {...this.props}>
        {this.props.children}
      </View>
    );
  }
}

FormGroup.propTypes = {
  ...View.propTypes
};

const AnimatedFormGroup = connectAnimation(FormGroup);
const StyledFormGroup = connectStyle('corvus.ui.FormGroup')(AnimatedFormGroup);

export {
  StyledFormGroup as FormGroup,
};
