# Fork Shoutem

Repositorio dentro de nuestra organización, actualizable desde shoutem github y personalizable para nuestras necesidades


# Registro de cambios

- Se eliminaron dependencias, para integrarlas al repositorio:
```
   "react-native-lightbox": "shoutem/react-native-lightbox#4e58b9b1aca166142d69b161ff511efc3358ff9f",
   "react-native-transformable-image": "shoutem/react-native-transformable-image#7d15395ec3857571711a12bc08f24f3a96ff1cde",
```
- Se remplazo Mocha Test por Jest Test, Dependencias
- Se Migro las pruebas de Mocha a Jest, Files
- Navigation, fue deprecado por esta razón se eliminaron 2 componentes relacionados a este

# RoadMap 

- A partir de la version 16 de reactjs proptypes, sera separada completamente de React,
por esta razón debemos migrar estos componentes usando la libreria PropTypes propia de facebook,
o usar flow o typescript para evitar el uso de estas librerias. 

![alt text](http://jlord.us/git-it/assets/imgs/clone.png)

# Corvus UI

Corvus UI is a set of styleable components that enables you to build beautiful React Native applications for iOS and Android. All of our components are built to be both composable and [customizable](http://github.com/corvus/theme). Each component has a predefined style that is compatible with the rest of the Corvus UI, which makes it possible to build complex components that look great without the need to manually define complex styles.

## Install

```
$ npm install --save @corvus/react-native-ui
$ react-native link # No need to run this with Exponent
```

## Docs

All the documentation is available on the [Developer portal](http://corvus.github.io/docs/ui-toolkit/introduction).

## Community

Join [our community](https://www.facebook.com/groups/corvus.community/) on Facebook. Also, feel free to ask a question on Stack Overflow using ["corvus" tag](http://stackoverflow.com/tags/corvus).

## Examples

To see how Corvus UI works, you can:

- include the `Examples` component into your React Native app or
- run `Restaurants` app in `examples` folder.

### Examples component

**If you are using Exponent, see [this
project](https://github.com/exponentjs/corvus-example/blob/master/main.js)
for example usage. Otherwise, follow the steps below.**

Create new React Native project and locate to it:

```bash
$ react-native init HelloWorld && cd HelloWorld
```

Install `@corvus/react-native-ui` in your project:

```bash
$ npm install --save @corvus/react-native-ui
$ react-native link
```

Now, simply copy the following to your `index.ios.js` files of the React Native project:

```JavaScript
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Examples } from '@corvus/react-native-ui';

class HelloWorld extends Component {
  render() {
    return (
      <Examples />
    );
  }
}

AppRegistry.registerComponent('HelloWorld', () => HelloWorld);
```

Finally, run the app!

```bash
$ react-native run-ios
```

To see other components, just import them from `@corvus/react-native-ui` and render them.

You can also use standard React Native components in your layouts anywhere you want, but they will not inherit either the theme or the parent styles, so you will need to style them manually.


### Restaurants app

Clone the [Corvus UI](https://github.com/corvus/ui) repository:

```bash
git clone https://github.com/corvus/ui.git
```

Locate to `RestaurantsApp` folder:

```bash
cd ui/examples/RestaurantsApp
```

Install and link dependencies:

```bash
npm install
react-native link
```

Finally, run the app!

```bash
react-native run-ios
react-native run-android
```

## UI Toolkit

Corvus UI is a part of the [Corvus UI Toolkit](https://corvus.github.io/ui/) that enables you to build professional looking React Native apps with ease.

It consists of three libraries:

- [@corvus/react-native-ui](https://github.com/corvus/ui): beautiful and customizable UI components
- [@corvus/react-native-theme](https://github.com/corvus/theme): “CSS-way” of styling entire app
- [@corvus/react-native-animation](https://github.com/corvus/animation): declarative way of applying ready-made  animations

